import {Builder, By, Key} from "selenium-webdriver";
import { should } from "chai";
should();

const url = "https://lambdatest.github.io/sample-todo-app/";
let driver;

beforeEach(function (){
    // Launch the browser
    driver = new Builder()
        .forBrowser("firefox")
        .build();
});

afterEach(async function () {
    // Close the browser
    await driver.quit();
})

//describe block
describe('Add todo tests', () => {
    it('should add a todo successfully', async function(){
        // Navigate to our url
        await driver.get(url);

        // Add a todo
        await driver
            .findElement(By.id("sampletodotext"))
            .sendKeys("Learn Selenium", Key.RETURN);

        // Assert
        let todoText = await driver
            .findElement(By.xpath("//li[last()]"))
            .getText()
            .then(function (value){
                return value;
            });

        // Assert using chai assertion
        todoText.should.equal("Learn Selenium");
    });
});




