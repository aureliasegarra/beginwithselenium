import {Builder, By, Key} from "selenium-webdriver";
import { should } from "chai";
should();


//describe block
describe('Add another todo tests', () => {
    it('should add another todo successfully', async function(){
        // Launch the browser
        let driver = new Builder().forBrowser("firefox").build();

        // Navigate to our url
        await driver.get("https://lambdatest.github.io/sample-todo-app/");

        // Add a todo
        await driver
            .findElement(By.id("sampletodotext"))
            .sendKeys("Learn Selenium", Key.RETURN);

        // Assert
        let todoText = await driver
            .findElement(By.xpath("//li[last()]"))
            .getText()
            .then(function (value){
                return value;
            });

        // Assert using chai assertion
        todoText.should.equal("Learn Selenium");

        // Close the browser
        await driver.quit();
    });

    it('should add a new todo successfully', async function(){
        // Launch the browser
        let driver = new Builder().forBrowser("firefox").build();

        // Navigate to our url
        await driver.get("https://lambdatest.github.io/sample-todo-app/");

        // Add a todo
        await driver
            .findElement(By.id("sampletodotext"))
            .sendKeys("Learn Javascript", Key.RETURN);

        // Assert
        let todoText = await driver
            .findElement(By.xpath("//li[last()]"))
            .getText()
            .then(function (value){
                return value;
            });

        // Assert using chai assertion
        todoText.should.equal("Learn Javascript");

        // Close the browser
        await driver.quit();
    });
});




